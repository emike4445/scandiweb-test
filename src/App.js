import Home from './Home';
import AddProduct from './AddProduct';
import './App.css';
import {  Routes, Route } from "react-router-dom";

function App() {

  return (
    <>
     
          <Routes>
          
                <Route exact path="/" element={<Home />} />

                <Route path="/addproduct" element={<AddProduct />} />         

                {/* <Route path="/home" component={Home} /> */}
            </Routes>


    </>
  );
}

export default App;
