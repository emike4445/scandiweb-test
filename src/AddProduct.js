import React from 'react'
import { useState } from 'react';
import './App.css';
import Footer from './Footer';
import {useNavigate} from 'react-router-dom';

export const AddProduct = () => {

    const navigate = useNavigate();

    const product={        
        sku:"",
        name:"",
        price:"",
        type:"",
        value:"",
    }

    let finalProduct={
        sku:"",
        name:"",
        price:"",
        type:"",
        value:"",
    }

    const description_obj={
        Book:"please provide weight (in KG)",
        DVD: "please provide size (in MB)",
        Furniture:"please provide dimensions (HxWxL)"
    }

    const type_map={
        DVD:"size",
        Book:"weight",
        Furniture:"Dimension"
    }
    
    const value_map={
        DVD:" MB",
        Book:" KG",
        Furniture: ""
        
    }

    const[formInputs, setFormInputs]= useState(product);
    
    const [error, setError]= useState('')
    const [description, setDescription]= useState('');
    const [type,setType]= useState("");
    
    
    const inputChange=(e)=>{
        const { name, value}= e.target;
        setFormInputs({...formInputs, [name]: value})
    };

    const typeChanger=(e)=>{
        setType(e.target.value);
        setDescription(description_obj[e.target.value])
        // if(e.target.value==="Book") {setDescription('please provide weight (in KG)')}
        // if(e.target.value==="DVD") {setDescription('please provide size (in MB)')}
        // if(e.target.value==="Furniture") {setDescription('please provide dimensions (HxWxL)')}

    }


    /**
     *  
     * HTML does the type validation
     *
     * approach here is to parse form input before submission 
     * to make back-end structure consistent
     */

    const save=()=>{
        //  console.log(formInputs)
        const inputFeilds = document.querySelectorAll("input");
        var validInputs = Array.from(inputFeilds).filter( input => input.parentElement.parentElement.style.display !== "none");
        validInputs = Array.from(validInputs).filter( input => input.parentElement.parentElement.parentElement.style.display !== "none");
        validInputs = Array.from(validInputs).every( input => input.value !== "");




        if( !validInputs || type===""){ setError("please fill all input feilds")}
        else{            
            setError('');
            
            finalProduct.sku=formInputs.sku;
            finalProduct.name=formInputs.name;
            finalProduct.price=formInputs.price;

            finalProduct.type= type_map[type];
            finalProduct.productType=type;         
            
            finalProduct.value= formInputs[type_map[type]] + value_map[type];

            if(document.getElementById("furniture").style.display !== "none"){
                finalProduct.height= formInputs.height;
                finalProduct.width= formInputs.width;
                finalProduct.length=formInputs.length;
            }

            // console.log(finalProduct);
             
            
            submit();

        }        
    }
 

    const submit= ()=>{
        fetch("http://0989uui.myartsonline.com/add.php",{
            method:'POST',
            headers:{
                "content-type":"application/json"
            },
            body: JSON.stringify(finalProduct)
        })
        .then(function(res){
            return res.text();
        })
        .then(function(data){
        
            // console.log(data)
            navigate('/')
            
        })
    }

    function cancel(e){
        setFormInputs({});
        navigate('/')
        // console.log(Router)
    }


    return (
        
    <div className= "container">
      <head>
        <title>Scandiweb test | Add Product</title>
        <meta name="description" content="Michael's Assessment" />
        <link rel="icon" href="/favicon.ico" />
      </head>
      
      <header className= "header_section">
        <h1 className= "title">
          Product Add
        </h1>

        <div className= "header_actions">
            
            <button onClick={save} >Save</button>
            <button href="/" onClick={cancel}>  Cancel</button>
        </div>
      </header>


      <main className= "main">

        <form className= "add_form" id="product_form">
            
            <div className= "add_form_normal">
                <label>SKU</label> <input id="sku" type="text" name="sku" onChange= {inputChange}/>
            </div>

            <div className= "add_form_normal">
                <label>Name</label> <input id="name" name="name" type="text"  onChange= {inputChange} />
            </div>

            <div className="add_form_normal">
                <label>Price ($)</label> <input id="price" name="price" type="number" onChange={inputChange}/>
            </div>

            <div className="add_form_normal">
                <label>Type</label> <select name="type" id="productType" onChange={typeChanger}>
                    <option value="">select Type</option>
                    <option id="DVD" value="DVD">DVD</option>
                    <option id="Furniture" value="Furniture">Furniture</option>
                    <option value="Book">Book</option>
                </select>
            </div>

            <div style={{
                    display: type==="DVD" ? "block" : "none",
            }}>
                <div>
                    <label>Size (MB)</label> <input id="size" name="size" type="number" onChange={inputChange}/>
                </div>
            </div>

            <div id="furniture" style={{
                display: type==="Furniture" ? "block" : "none",
            }}>
                <div style={{
                     display: "flex",
                     flexDirection: "column",
                     gap:"30px",
                }}>
                    <div><label>Height (CM)</label> <input id="height" type="number" name="height" onChange={inputChange} /></div>
                    <div><label>Width (CM)</label> <input id="width" type="number" name="width" onChange={inputChange} /></div>
                    <div><label>Length (CM)</label> <input id="length" type="number" name="length"  onChange={inputChange} /></div>
                </div>
            </div>

            <div style={{
                display: type==="Book" ? "block" : "none",
            }}>
                <div>
                    <label>Weight (KG)</label> 
                    <input id="weight" type="number" name="weight" onChange={inputChange}/>
                </div>
            </div>
            <span>{description}</span>
            <span style={{
                width:"400px",
                height:"40px",
                marginTop:"10px",
                backgroundColor:"red",
                color:"white",
                padding:"10px",
                display: error!==""? "block" : "none",
            }}>
                {error}
            </span>
        </form>


      </main>

      <Footer />
    
    </div>
    )
}

export default AddProduct;