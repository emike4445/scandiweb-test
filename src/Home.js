import { useState,useEffect } from 'react';
import Footer from './Footer';
import './App.css';
import { Link } from "react-router-dom";

function Home() {

    const [products, setProducts] = useState([]);
    
    useEffect(()=>{
        const getProducts = async ()=>{
            const res= await fetch("http://0989uui.myartsonline.com/getAll.php");
            //console.log(res.text())
            let json= await res.json();
            json= Object.values(json);
            //return json;
            setProducts(json)
        }
        getProducts();        

    },[])
    


    var finalDeleteProducts=[]
      
    const mass_delete =()=>{
  
      finalDeleteProducts=[];

      var deleteInputs = document.querySelectorAll('input');
      var deleteProducts= Array.from(deleteInputs);
      deleteProducts=deleteProducts.filter((checkbox)=> checkbox.checked);
      // console.log(deleteProducts);
      deleteProducts.map(function(input){
        let sku= ((input.parentElement.children)[1].innerText);
        let name= ((input.parentElement.children)[2].innerText);
        finalDeleteProducts.push({
          sku: sku,
          name: name,
        })
      })
  
    
      make_delete_request()
     
    }

    function make_delete_request(){
        fetch("http://0989uui.myartsonline.com/delete.php",{
          method:'POST',
          headers:{
              "content-type":"application/json"
          },
          body: JSON.stringify(finalDeleteProducts)
          })
          .then(function(res){
              return res.text();
          })
          .then(function(data){
             update();
               //router.push('/');
              // console.log(data);
          })
      }

      const update= ()=>{

        let updatedProducts= products.filter((product)=>{
          return !finalDeleteProducts.some((deleteproduct)=>{
              return product.sku === deleteproduct.sku
          })
        })

        console.log(updatedProducts)
        setProducts(updatedProducts)
      }


  return (
    <div className="container">
      <header className="header_section">
        <h1 className="title">
            Product List
            </h1>

            <div className="header_actions" >
                <Link to="/addproduct" >
                    ADD
                </Link>
                

                <button onClick={mass_delete}> MASS DELETE</button>
            </div>
      </header>

        
        <main className="main">

            { products.length? 

              (  products.map((product,index)=>(
                    <div key={product.sku} className="product_card">
                        <input type="checkbox" id="delete-checkbox" databird-input-type="delete-checkbox" className="delete-checkbox"></input>
                        
                        <span>{product.sku}</span>
                        <span>{product.name}</span>
                        <span>{"$"+product.price}</span>
                        <div>
                        <span>{product.type}: </span>
                        <span>{product.value}</span>
                        </div>
                    </div>
                ))
              )
              :("no products to show")
                }

            {/* {console.log(products)} */}

        </main>
        
        <Footer />
    
    </div>
  );
}

export default Home;
