<?php
require_once('includes/autoloader.php');


class app{

    /**
     * public function get_all_products and mass_delete instantiate dummy products because all products
     * use same table and only sku is needed to delete
     * 
     * 
     * if products are to use individual tables these functions could be modified to instantiate objects of product type
     * like implemented in save_product funcion
     */


    public function get_all_products(){
        $dummy_product= new book();
        return $dummy_product->get_all_products();
    }

    public function save_product(Array $product){

        //echo strtolower($product['productType']);
        $product_classname = strtolower($product['productType']);
        $product_object =new $product_classname;
        $product_object->instantiateMyProperties($product);
        //$product_object->iterateAllPropertiesa();
        // var_dump($product_object);

        
        if($product_object->save()){
            return json_encode("success");
        }
        else{
            return json_encode("error");
        }
    }


    public function mass_delete(Array $products){
      
       // var_dump($products);        
        foreach($products as $product){
            $product= (array) $product;
            $dummy_product= new book();
            $dummy_product->instantiateMyProperties($product);
            //var_dump($dummy_product);

            $dummy_product->delete();
          
        } 
    }

    
}