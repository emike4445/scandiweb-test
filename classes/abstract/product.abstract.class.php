<?php

require_once('./includes/autoloader.php');


/**
 * abstract product class extended by
 * this class could be implemented as a standalone orm
 * 
 * 
 * all other product types extends this class and input parameters parsed to suit consistent database structure
 * 
 * else each product type class could have its own table name
 */

abstract class product extends database{

    protected $tableName="products";
    protected $conn;

    public function __construct(){
        $this->conn= $this->connect();
    }

    public function instantiateMyProperties($props)
    {
       foreach ($this as $key => $value) {

        if($key=="conn"){continue;}
            if (array_key_exists($key, $props)) {
                //$this->$key=$props[$key];
                $this->__set($key,$props[$key]);
            }
        }
    }

    public function get_all_products(){
        $sql="SELECT * FROM Products";
        $prep=$this->conn->prepare($sql);
        $prep->execute();
        $result=$prep->fetchAll();
        return $result;
    }


    public function delete(){
        $sql ="DELETE  FROM $this->tableName WHERE sku =:sku ";
        $prep= $this->conn->prepare($sql);
        return $prep->execute(['sku'=>$this->sku]);
    }


    /**
     * other crud functionalitites could be implemented here
     * 
     */


    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
}

?>